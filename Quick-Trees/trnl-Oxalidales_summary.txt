;   MEGA-CC (Molecular Evolutionary Genetics Analysis Compute Core)
;
;   Suggested Citation for MEGA-CC 7:
;
;   Kumar S, Stecher G, Tamura K (2016)
;   MEGA7: Molecular Evolutionary Genetics Analysis 7.0 for bigger datasets
;   Molecular Biology and Evolution 33:1870-1874
;
;   Kumar S, Stecher G, Peterson D, and Tamura K (2012)
;   MEGA-CC: Computing Core of Molecular Evolutionary Genetics
;   Analysis Program for Automated and Iterative Data Analysis.
;   Bioinformatics (2012) 28:2685-2686 (Epub 2012 Aug 24)

[General Info]

	Data Type                               = nucleotide (non-coding)
	No. of Taxa                             =        5
	No. of Sites                            =     1100
	Data File                               = 'trnl-Oxalidales.MAFFT.fas'
	Settings File                           = './infer_ML_nucleotide.mao'
	Command Line                            = /usr/bin/megacc -a infer_ML_nucleotide.mao -d trnl-Oxalidales.MAFFT.fas

[Analysis Settings]

	Analysis                                = Phylogeny Reconstruction
	Statistical Method                      = Maximum Likelihood
	Test of Phylogeny                       = None
	No. of Bootstrap Replications           = Not Applicable
	Substitutions Type                      = Nucleotide
	Model/Method                            = General Time Reversible model
	Rates among Sites                       = Gamma Distributed With Invariant Sites (G+I)
	No of Discrete Gamma Categories         = 5
	Gaps/Missing Data Treatment             = Use all sites
	Site Coverage Cutoff (%)                = Not Applicable
	ML Heuristic Method                     = Nearest-Neighbor-Interchange (NNI)
	Initial Tree for ML                     = Make initial tree automatically (Default - NJ/BioNJ)
	Branch Swap Filter                      = None
	Number of Threads                       = 8
	Has Time Limit                          = False
	Maximum Execution Time                  = -1
	datatype                                = snNucleotide
	containsCodingNuc                       = False
	MissingBaseSymbol                       = ?
	IdenticalBaseSymbol                     = .
	GapSymbol                               = -

[Analysis Statistics]

	Start Time                              = 9-1-18 22:00:56
	End Time                                = 9-1-18 22:00:56
	Execution Time                          =    0.554 (seconds)
	Peak Memory Used(Working Set)           = Not available
	Sum of branch lengths                   =    2.368
	Model                                   = General Time Reversible model (+G+I)
	Num of params                           =       17
	Num of rates                            =        5
	AICc                                    = 5737.820
	BIC                                     = 5846.350
	LnL                                     = -2851.841
	Invar                                   =    0.112
	Gamma                                   =    2.210
	Ts/Tv                                   =    3.223
