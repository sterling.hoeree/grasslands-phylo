;   MEGA-CC (Molecular Evolutionary Genetics Analysis Compute Core)
;
;   Suggested Citation for MEGA-CC 7:
;
;   Kumar S, Stecher G, Tamura K (2016)
;   MEGA7: Molecular Evolutionary Genetics Analysis 7.0 for bigger datasets
;   Molecular Biology and Evolution 33:1870-1874
;
;   Kumar S, Stecher G, Peterson D, and Tamura K (2012)
;   MEGA-CC: Computing Core of Molecular Evolutionary Genetics
;   Analysis Program for Automated and Iterative Data Analysis.
;   Bioinformatics (2012) 28:2685-2686 (Epub 2012 Aug 24)

[General Info]

	Data Type                               = nucleotide (non-coding)
	No. of Taxa                             =       11
	No. of Sites                            =     1001
	Data File                               = '../GUIDANCE-Aligned-Fasta/its-Myrtales.MAFFT.aln.With_Names.fas'
	Settings File                           = '/home/shoeree/infer_ML_nucleotide.mao'
	Command Line                            = /usr/bin/megacc --analysisOptions /home/shoeree/infer_ML_nucleotide.mao --data ../GUIDANCE-Aligned-Fasta/its-Myrtales.MAFFT.aln.With_Names.fas --outfile its-Myrtales.tre

[Analysis Settings]

	Analysis                                = Phylogeny Reconstruction
	Statistical Method                      = Maximum Likelihood
	Test of Phylogeny                       = None
	No. of Bootstrap Replications           = Not Applicable
	Substitutions Type                      = Nucleotide
	Model/Method                            = General Time Reversible model
	Rates among Sites                       = Gamma Distributed With Invariant Sites (G+I)
	No of Discrete Gamma Categories         = 5
	Gaps/Missing Data Treatment             = Use all sites
	Site Coverage Cutoff (%)                = Not Applicable
	ML Heuristic Method                     = Nearest-Neighbor-Interchange (NNI)
	Initial Tree for ML                     = Make initial tree automatically (Default - NJ/BioNJ)
	Branch Swap Filter                      = None
	Number of Threads                       = 8
	Has Time Limit                          = False
	Maximum Execution Time                  = -1
	datatype                                = snNucleotide
	containsCodingNuc                       = False
	MissingBaseSymbol                       = ?
	IdenticalBaseSymbol                     = .
	GapSymbol                               = -

[Analysis Statistics]

	Start Time                              = 21-12-17 16:58:09
	End Time                                = 21-12-17 16:58:58
	Execution Time                          =   48.903 (seconds)
	Peak Memory Used(Working Set)           = Not available
	Sum of branch lengths                   =   10.891
	Model                                   = General Time Reversible model (+G+I)
	Num of params                           =       29
	Num of rates                            =        5
	AICc                                    = 8436.372
	BIC                                     = 8628.864
	LnL                                     = -4189.033
	Invar                                   = n/a
	Gamma                                   =    0.593
	Ts/Tv                                   =    1.689
