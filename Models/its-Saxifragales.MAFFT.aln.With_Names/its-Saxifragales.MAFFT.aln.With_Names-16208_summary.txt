;   MEGA-CC (Molecular Evolutionary Genetics Analysis Compute Core)
;
;   Suggested Citation for MEGA-CC 7:
;
;   Kumar S, Stecher G, Tamura K (2016)
;   MEGA7: Molecular Evolutionary Genetics Analysis 7.0 for bigger datasets
;   Molecular Biology and Evolution 33:1870-1874
;
;   Kumar S, Stecher G, Peterson D, and Tamura K (2012)
;   MEGA-CC: Computing Core of Molecular Evolutionary Genetics
;   Analysis Program for Automated and Iterative Data Analysis.
;   Bioinformatics (2012) 28:2685-2686 (Epub 2012 Aug 24)

[General Info]

	Data Type                               = nucleotide (non-coding)
	No. of Taxa                             =       13
	No. of Sites                            =      981
	Data File                               = 'its-Saxifragales.MAFFT.aln.With_Names.fas'
	Settings File                           = '/tmp/model_sel_ml_nucleotide.mao'
	Command Line                            = /usr/bin/megacc -a /tmp/model_sel_ml_nucleotide.mao -d its-Saxifragales.MAFFT.aln.With_Names.fas -o /tmp/its-Saxifragales.MAFFT.aln.With_Names.fas/

[Analysis Settings]

	Analysis                                = Model Selection (ML)
	Tree to Use                             = Automatic (Neighbor-joining tree)
	Statistical Method                      = Maximum Likelihood
	Substitutions Type                      = Nucleotide
	Gaps/Missing Data Treatment             = Use all sites
	Site Coverage Cutoff (%)                = Not Applicable
	Branch Swap Filter                      = None
	Has Time Limit                          = False
	Maximum Execution Time                  = -1
	datatype                                = snNucleotide
	containsCodingNuc                       = False
	MissingBaseSymbol                       = ?
	IdenticalBaseSymbol                     = .
	GapSymbol                               = -

[Analysis Statistics]

	Start Time                              = 29-12-17 02:28:54
	End Time                                = 29-12-17 02:30:13
	Execution Time                          =   78.185 (seconds)
	Peak Memory Used(Working Set)           = Not available
