;   MEGA-CC (Molecular Evolutionary Genetics Analysis Compute Core)
;
;   Suggested Citation for MEGA-CC 7:
;
;   Kumar S, Stecher G, Tamura K (2016)
;   MEGA7: Molecular Evolutionary Genetics Analysis 7.0 for bigger datasets
;   Molecular Biology and Evolution 33:1870-1874
;
;   Kumar S, Stecher G, Peterson D, and Tamura K (2012)
;   MEGA-CC: Computing Core of Molecular Evolutionary Genetics
;   Analysis Program for Automated and Iterative Data Analysis.
;   Bioinformatics (2012) 28:2685-2686 (Epub 2012 Aug 24)

[General Info]

	Data Type                               = nucleotide (non-coding)
	No. of Taxa                             =       16
	No. of Sites                            =     1140
	Data File                               = 'trnl-Apiales.MAFFT.aln.With_Names.fas'
	Settings File                           = '/tmp/model_sel_ml_nucleotide.mao'
	Command Line                            = /usr/bin/megacc -a /tmp/model_sel_ml_nucleotide.mao -d trnl-Apiales.MAFFT.aln.With_Names.fas -o /tmp/trnl-Apiales.MAFFT.aln.With_Names.fas/

[Analysis Settings]

	Analysis                                = Model Selection (ML)
	Tree to Use                             = Automatic (Neighbor-joining tree)
	Statistical Method                      = Maximum Likelihood
	Substitutions Type                      = Nucleotide
	Gaps/Missing Data Treatment             = Use all sites
	Site Coverage Cutoff (%)                = Not Applicable
	Branch Swap Filter                      = None
	Has Time Limit                          = False
	Maximum Execution Time                  = -1
	datatype                                = snNucleotide
	containsCodingNuc                       = False
	MissingBaseSymbol                       = ?
	IdenticalBaseSymbol                     = .
	GapSymbol                               = -

[Analysis Statistics]

	Start Time                              = 29-12-17 02:31:29
	End Time                                = 29-12-17 02:35:09
	Execution Time                          =  219.713 (seconds)
	Peak Memory Used(Working Set)           = Not available
